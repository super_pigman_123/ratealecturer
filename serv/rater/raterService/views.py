from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from django.core.exceptions import ObjectDoesNotExist

import random

from .models import *
import json

def homepage(request):
	return HttpResponse("Homepage")


@csrf_exempt
def register(request):
	if request.method == "POST":
		data = json.loads(request.body)
		usrnm = data['username']
		email = data['email']
		pwd = data['pwd']
		if ( User.objects.filter(username = usrnm).exists() ):
			response = HttpResponse("Registration failure, username already taken")
			response.status_code = 409
			response.reason_phrase = 'CONFLICT'
		else:
			user = User.objects.create_user(username=usrnm,
											email=email,
											password=pwd)
			response = HttpResponse("Registration success")
			response.status_code = 200
			response.reason_phrase = 'OK'
		return response
	else:
		response = HttpResponse("Bad request")
		response.status_code = 400
		response.reason_phrase = 'Bad request'
		return response


@csrf_exempt
def log_in(request):
	if request.method == "POST":
		data = json.loads(request.body)
		usrnm = data['username']
		pwd = data['pwd']

		user = authenticate(request, username=usrnm, password=pwd)
		if user is not None:
			login(request, user)
			sesh = Session(sessionId = random.randint(0,255))
			sesh.save()
			payload = json.dumps({'text': "You are now logged in.", 'sessionId':sesh.sessionId})
			response = HttpResponse(payload)
			response.status_code = 200
			response.reason_phrase = 'OK'
		else:
			response = HttpResponse("Login failure, try again.")
			response.status_code = 409
			response.reason_phrase = 'CONFLICT'
		return response
	else:
		response = HttpResponse("Bad request.")
		response.status_code = 400
		response.reason_phrase = 'Bad request'
		return response


@csrf_exempt
def log_out(request):
	if request.method == "POST":
		data = json.loads(request.body)
		sid = data.get("sid", -1)
		if Session.objects.filter(sessionId = sid).exists() :
			Session.objects.get(sessionId = sid).delete()

			response = HttpResponse("You are now logged out.")
			response.status_code = 200
			response.reason_phrase = 'OK'
		else:
			response = HttpResponse("Logout failure, try again.")
			response.status_code = 409
			response.reason_phrase = 'CONFLICT'
		return response
	else:
		response = HttpResponse("Bad request.")
		response.status_code = 400
		response.reason_phrase = 'Bad request'
		return response

def get_module_list(request):
    if request.method == "GET":
        # (1) Convert 'Teaching' instances into module instances by merging some
        module_instances = []
        for t in Teaching.objects.values():

            module_instance_exists = False
            for m in module_instances:
                if (t['module_id'] == m['module_id']):
                    if (t['year'] == m['year']):
                        if (t['semester'] == m['semester']):
                            module_instance_exists = True
                            break

            if(module_instance_exists):
                m['professor_id'] = [m['professor_id'], t['professor_id']]
            else:
                module_instances.append({'module_id': t['module_id'],'year': t['year'], 'semester':t['semester'], 'professor_id':t['professor_id']})

        # (2) Add useful data based on module_id/professor_id
        for m in module_instances:
            module = Module.objects.get(id = m['module_id'])
            m['module'] = module.title
            m['module_dbid'] = module.identifier

            m['professors'] = []
            m['professors_dbid'] = []

            if isinstance(m['professor_id'], list):
                for pid in m['professor_id']:
                    prof = Professor.objects.get(id = pid)
                    m['professors'].append(prof.name)
                    m['professors_dbid'].append(prof.identifier)
            else:
                prof = Professor.objects.get(id = m['professor_id'])
                m['professors'].append(prof.name)
                m['professors_dbid'].append(prof.identifier)


        data = {}
        data['count'] = len(module_instances)
        for x in range(len(module_instances)):
            data['module' + str(x)] = module_instances[x]
        payload = json.dumps(data)
        response = HttpResponse(payload)
        response.status_code = 200
        response.reason_phrase = 'OK'
        return response

    else:
        response = HttpResponse("Bad request.")
        response.status_code = 400
        response.reason_phrase = 'Bad request'
        return response


def get_rating_list(request):
	if request.method == "GET":
		rating_data = {}
		rating_data['count'] = Professor.objects.count()
		for p in Professor.objects.values():
			avg_rating_per_module = []
			for t in Teaching.objects.filter(professor=Professor.objects.get(id=p['id'])):
			    if(int(t.numOfRatings) > 0):
				    avg_rating_per_module.append(int(t.total_ratings)/int(t.numOfRatings))
			rating_per_prof = {}
			rating_per_prof['uid'] = p['identifier']
			rating_per_prof['name'] = p['name']
			if (len(avg_rating_per_module)>0):
			    rating_per_prof['avg_rating'] = str(int(sum(avg_rating_per_module) / len(avg_rating_per_module)))
			else :
			    rating_per_prof['avg_rating'] = 'n/a'
			rating_data['professor' + str(p['id'])] = rating_per_prof
		payload = json.dumps(rating_data)

		response = HttpResponse(payload)
		response.status_code = 200
		response.reason_phrase = 'OK'
		return response

	else:
		response = HttpResponse("Bad request.")
		response.status_code = 400
		response.reason_phrase = 'Bad request'
		return response


def get_avg_for(request):
	if request.method == "GET":
		# Get params
		prof_id = request.GET.get('prof_id', 'notfound')
		module_id = request.GET.get('module_id', 'notfound')
		avg_rating_per_module = []
		try:
			prof = Professor.objects.get(identifier = prof_id)
			module = Module.objects.get(identifier = module_id)
		except ObjectDoesNotExist:
			response = HttpResponse('Not found')
			response.status_code = 404
			response.reason_phrase = 'Not found'
			return response
		if (Teaching.objects.filter(professor = prof, module = module).exists()):
			t = Teaching.objects.get(professor = prof, module = module)
			if (int(t.numOfRatings) > 0):
			    avg_rating = int(t.total_ratings)/int(t.numOfRatings)
			else:
			    avg_rating = 'n/a'

			returnable = {}
			returnable['name'] = prof.name
			returnable['prof_id'] = prof_id
			returnable['module'] = module.title
			returnable['module_id'] = module_id
			returnable['avg_rating'] = avg_rating

			payload = json.dumps(returnable)
			response = HttpResponse(payload)
			response.status_code = 200
			response.reason_phrase = 'OK'
		else :
			response = HttpResponse('No ratings found')
			response.status_code = 404
			response.reason_phrase = 'Not found'
		return response

	else:
		response = HttpResponse("Bad request.")
		response.status_code = 400
		response.reason_phrase = 'Bad request'
		return response

@csrf_exempt
def rate(request):
	if request.method == "POST":
		data = json.loads(request.body)
		sid = data.get("sid", -1)
		if Session.objects.filter(sessionId = sid).exists() :
			prof_id = data.get('prof_id', 'notfound')
			module_id = data.get('module_id', 'notfound')
			year = data.get('year', 'notfound')
			sem = data.get('sem', 'notfound')
			new_rating = data.get('rating', 0)
			try:
				prof = Professor.objects.get(identifier = prof_id)
				module = Module.objects.get(identifier = module_id)
			except ObjectDoesNotExist:
				response = HttpResponse('Not found')
				response.status_code = 404
				response.reason_phrase = 'Not found'
				return response

			if (Teaching.objects.filter(professor = prof, module = module, year=year, semester = sem).exists()):
				t = Teaching.objects.get(professor = prof, module = module, year=year, semester = sem)
				t.total_ratings = int(t.total_ratings) + int(new_rating)
				t.numOfRatings = int(t.numOfRatings) + 1;
				t.save()
			else :
				response = HttpResponse('Professor doesn\'t teach this module.')
				response.status_code = 404
				response.reason_phrase = 'Not found'
				return response

			response = HttpResponse("Rating submitted")
			response.status_code = 200
			response.reason_phrase = 'OK'
			return response
		else:
			response = HttpResponse("Please log in to use the service.")
			response.status_code = 400
			response.reason_phrase = 'Bad request'
			return response
	else:
		response = HttpResponse("Bad request.")
		response.status_code = 400
		response.reason_phrase = 'Bad request'
		return response
