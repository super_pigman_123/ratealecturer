from django.db import models
from django.contrib.auth.models import User

from django.core.validators import MaxValueValidator, MinValueValidator

class Module(models.Model):
	identifier = models.CharField(max_length = 3, unique = True)
	title = models.CharField(max_length = 140)

	def __str__(self):
		return self.title

class Professor(models.Model):
	identifier = models.CharField(max_length = 3, unique = True)
	name = models.CharField(max_length = 140)
	teaches = models.ManyToManyField(Module, through='Teaching')

	def __str__(self):
		return self.name

class Teaching(models.Model):
	professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
	module = models.ForeignKey(Module, on_delete=models.CASCADE)
	year = models.IntegerField()
	semester = models.IntegerField(default = 1, validators=[MinValueValidator(1), MaxValueValidator(2)])

	total_ratings = models.IntegerField(default=0, validators=[MinValueValidator(0)])
	numOfRatings = models.IntegerField(default=0,validators=[MinValueValidator(0)])

	def __str__(self):
		return str(self.professor) + ' - ' + str(self.module) + ' - ' + str(self.total_ratings) + '/' + str(self.numOfRatings)

class Session(models.Model):
	sessionId = models.IntegerField(validators=[MaxValueValidator(255)])
