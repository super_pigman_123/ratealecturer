from django.apps import AppConfig


class RaterserviceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'raterService'
