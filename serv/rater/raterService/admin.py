from django.contrib import admin
from .models import Module, Professor, Teaching, Session

# Register your models here.
admin.site.register(Session)
admin.site.register(Module)
admin.site.register(Professor)
admin.site.register(Teaching)

