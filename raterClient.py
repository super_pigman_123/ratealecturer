import sys
import requests
import json

url = 'http://sc18mg.pythonanywhere.com/'
sessionId = -1;

def register():
	registerUrl = url + 'register/'
	usrname = input("Username: ")
	email = input("Email:")
	pwd = input("Password:")
	
	payload={'username':usrname,'email':email,'pwd':pwd}
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(registerUrl, data=json.dumps(payload), headers=headers)
	print(r.text)


def login():
	loginUrl = url + 'login/'
	usrname = input("Username: ")
	pwd = input("Password:")
	
	payload={'username':usrname,'pwd':pwd}
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(loginUrl, data=json.dumps(payload), headers=headers)
	
	response = json.loads(r.content)
	print(response['text'])
	if (r.status_code == 200):
		global sessionId
		sessionId = response['sessionId']


def logout():
	loginUrl = url + 'logout/'
	
	global sessionId
	payload={'sid':sessionId}
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	r = requests.post(loginUrl, data=json.dumps(payload), headers=headers)
	print(r.text)
	
	sessionId = -1


def moduleList():
	listUrl = url + 'list/'
	headers = {'Content-type': 'application/json', 'Accept': 'text/json'}
	r = requests.get(listUrl, headers=headers)
	
	module_data = json.loads(r.content)
	count = module_data['count']
	print('==================')
	for i in range(0,count):
		module = module_data['module' + str(i)]
		print("Module ID : ",str(module['module_dbid']))
		print("Module Title : ",module['module'])
		print("Module year, semester : ",str(module['year']),',' ,str(module['semester']) )
		print("Taught by : ", end = '') 
		professors = module['professors']
		for i in range(len(professors)) :
			print(professors[i],'(%s)' % module['professors_dbid'][i],end = ' ')
		print('\n==================')

	
def ratingList():
	ratingsUrl = url + 'ratings/'
	headers = {'Content-type': 'application/json', 'Accept': 'text/json'}
	r = requests.get(ratingsUrl, headers=headers)
	
	ratings_data = json.loads(r.content)
	count = ratings_data['count']
	for i in range(0,count):
		prof = ratings_data['professor' + str(i)]
		print(prof['uid'], "-", prof['name'], "average rating", prof['avg_rating'], "/5")


def avgFor(command):
	ratingsUrl = url + 'avg/'
	headers = {'Content-type': 'application/json', 'Accept': 'text/json'}
	requested = {'prof_id':command[1],'module_id':command[2]}
	r = requests.get(ratingsUrl, headers=headers, params=requested)
	
	if (r.status_code == 200):
		rating_data = json.loads(r.content)
		print('Rating of', rating_data['name'],"(%s)"%rating_data['prof_id'], 'in module', rating_data['module'],"(%s)"%rating_data['module_id'], 'is', rating_data['avg_rating'])
	else:
		print(r.text)
		

def rate(command):
	ratingsUrl = url + 'submit_rating/'
	headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	global sessionId
	values = {'sid':sessionId, 'prof_id':command[1],'module_id':command[2], 'year':command[3], 'sem':command[4], 'rating':command[5]}
	r = requests.post(ratingsUrl, headers=headers, data=json.dumps(values))
	
	print(r.text)


while (True):
	command = input(">>").split()
	commlen = len(command)
	if (command[0] == 'register'):
		register()
	elif (command[0] == 'login'):
		login()
	elif (command[0] == 'logout'):
		logout()
	elif (command[0] == 'list'):
		moduleList()
	elif (command[0] == 'view'):
		ratingList()
	elif (command[0] == 'average'):
		if (commlen == 3):
			avgFor(command)
		else:
			print('Usage : average professor_id module_id')
	elif (command[0] == 'rate'):
		if (commlen == 6):
			rate(command)
		else:
			print('Usage : rate professor_id module_id year semester rating[1-5]')
	elif (command[0] == 'q'):
		break
	else:
		print('unrecognised command; try again')
logout()
