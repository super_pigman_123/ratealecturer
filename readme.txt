(1) Using raterClient.py
Launch using 'python3 raterClient.py', which will prompt you for a command.

List of commands:
- register
- login
- logout
- list
- view
- average <professor_id> <module_id>
- rate <professor_id> <module_id> <module_year> <module_sem> <rating_value>
These function like specified in the .docx
*using 'login' will only keep the user logged in while .py is running; restarting it will
require to re-login.
Lastly, there is 'q' command which logs out and closes the prompt.

(2) Domain : sc18mg.pythonanywhere.com
It is hard-coded into the client, you will not need to specify it anywhere.

(3) Admin account
username : ammar
password : readme.txt
